import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // TODO: 3. [hands-on] Code Suggestions 의 도움을 받아 ValidationPipe 를 추가해보세요.
  // TODO: 4. [hands-on] Code Suggestions 의 도움을 받아 ValidationPipe의 세부 설정을 추가해보세요.

  await app.listen(3000);
}
bootstrap();
